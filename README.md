# Research and Professional Ethics


## Professional Ethics

    • Professional Societies (ICS, IEEE , ACM)
    • Ethical Research and Implications (Computing, Machine Learning, Cognitive Systems...etc)
    • Legal Considerations
    • Intellectual Property

## IT governance, risk, and compliance

    • Information Security (ISO)
    • Data Protection (GDPR)
    • IT Governance and Risk Management (COBIT, ITIL)

## Designing a Research Project

    • Methodology and rationale
    • Creating a conceptual framework
    • The research statement/question/hypothesis/feasibility/ considering business intelligence aims
    • Research aims, objectives & deliverables

## Critically Reviewing the Literature

    • Literature Sources and Quality
    • Data sourcing and analysis
    • Critical Appraisal and Synthesis
    • Technical Writing (Audience based)
    • Source referencing (academic standard)

## Communication and Team Dynamics

    • Professional Communication
    • Team Building and Development
    • Cultural Diversity
    • Collaboration through Technology

## Self-Development & Learning

    • Skills Audit and Gap Analysis
    • Continuing Professional Development
    • e-Portfolios
